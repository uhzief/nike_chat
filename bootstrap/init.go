package bootstrap

import (
	"fmt"
	"log"
	"nakeChat_fuckkkk/db/mysqlDb"
	"nakeChat_fuckkkk/global/the_errors"
	"nakeChat_fuckkkk/global/variable"
	websocket2 "nakeChat_fuckkkk/middleware/websocket"
	"nakeChat_fuckkkk/utils/yml_conf"
	"nakeChat_fuckkkk/utils/zap_factory"
	"os"
)

// 检查必须的非贬义目录是否存在，避免编译后调用的时候缺失相关目录
func checkRequiredFolders() {
	//1.检查配置文件是否存在
	fmt.Println(variable.BasePath)
	if _,err:=os.Stat(variable.BasePath+"/config/config.yml");err!=nil{
		log.Fatal(the_errors.ErrorsConfigYamlNotExists + err.Error())
	}
	//2.检查public目录是否存在
	if _, err := os.Stat(variable.BasePath + "/public/"); err != nil {
		log.Fatal(the_errors.ErrorsPublicNotExists + err.Error())
	}
	//3.检查storage/logs 目录是否存在
	if _, err := os.Stat(variable.BasePath + "/storage/logs/"); err != nil {
		log.Fatal(the_errors.ErrorsStorageLogsNotExists + err.Error())
	}
	// 4.自动创建软连接、更好的管理静态资源
}

// 项目初始化工作

func init() {
	checkRequiredFolders()

	// 4.启动针对配置文件(confgi.yml)变化的监听， 配置文件操作指针，初始化为全局变量
	variable.ConfigYml = yml_conf.CreateYamlFactory()
	variable.ConfigYml.ConfigFileChangeListen()
	// 5.初始化全局日志句柄，并载入日志钩子处理函数
	variable.ZapLog = zap_factory.CreateZapFactory(zap_factory.ZapLogHandler)
	// 根据配置初始化mysql
	if variable.ConfigYml.GetInt("Mysql.IsInitGolobalGormMysql") == 1 {
		mysqldb, err := mysqlDb.GetMysqldb()
		if err != nil {
			log.Fatal(the_errors.ErrorsGormInitFail + err.Error())
		}
		variable.GormDbMysql = mysqldb
	}
	// 根据配置初始化mongodb
	if variable.ConfigYml.GetInt("MongoDB.Start") == 1 {

	}

	// 8.websocket Hub中心启动
	if variable.ConfigYml.GetInt("Websocket.Start") == 1 {  // 配置启动
		// websocket 管理中心hub全局初始化一份
		variable.WebsocketHub = websocket2.CreateHubFactory()
		if wh, ok := variable.WebsocketHub.(*websocket2.Hub); ok {
			go wh.Run()
		}
	}
}

