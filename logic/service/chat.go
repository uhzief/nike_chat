package service

import (
	"nakeChat_fuckkkk/global/variable"
	"net"
)

/**
聊天服务
 */

// InitChatServe 初始化聊天服务
func InitChatServe() net.Listener {
	listener, err := net.Listen("tcp", "0.0.0.0:8888")
	if err!=nil {
		variable.ZapLog.Error("聊天服务初始化失败")
	}
	return listener
}
