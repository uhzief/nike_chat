package model

import (
	"fmt"
	"gorm.io/gorm"
	"nakeChat_fuckkkk/global/the_errors"
	"nakeChat_fuckkkk/global/variable"
	"strings"
	"time"
)

/**
模型层基类
主要封装一些通用的数据库访问
*/

type BaseModel struct {
	Id        int `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"` // todo 代码中生成string类型时间戳
}

// IBase 封装通用方法
type IBase interface {
	Add(interface{}) error
	Delete(interface{}) error
	Update()
	GetById() interface{} // 根据id查询
}


func UseDbConn(sqlType string) *gorm.DB {
	var db *gorm.DB
	sqlType = strings.Trim(sqlType, " ")
	//if sqlType == "" {
	//	sqlType = variable.ConfigYml.GetString("UseDbType")
	//}
	switch strings.ToLower(sqlType) {
	case "mysql":
		if variable.GormDbMysql == nil {
			variable.ZapLog.Fatal(fmt.Sprintf(the_errors.ErrorsGormNotInitGlobalPointer, sqlType, sqlType))
		}
		db = variable.GormDbMysql
	//case "sqlserver":
	//	if variable.GormDbSqlserver == nil {
	//		variable.ZapLog.Fatal(fmt.Sprintf(my_errors.ErrorsGormNotInitGlobalPointer, sqlType, sqlType))
	//	}
	//	db = variable.GormDbSqlserver
	//case "postgres", "postgre", "postgresql":
	//	if variable.GormDbPostgreSql == nil {
	//		variable.ZapLog.Fatal(fmt.Sprintf(my_errors.ErrorsGormNotInitGlobalPointer, sqlType, sqlType))
	//	}
	//	db = variable.GormDbPostgreSql
	default:
		variable.ZapLog.Error(the_errors.ErrorsDbDriverNotExists + sqlType)
	}
	return db
}
