package model

import "nakeChat_fuckkkk/global/variable"

// CreateUserFactory 创建 userFactory
// 参数说明： 传递空值，默认使用 配置文件选项：UseDbType（mysql）
//func CreateUserFactory(sqlType string) *Users {
//	return &Users{
//		BaseModel: BaseModel{
//			DB: UseDbConn(sqlType),
//		},
//	}
//}

// Users 用户模型层
type Users struct {
	BaseModel        // 继承BaseModel
	Username  string `gorm:"column:username" json:"username"`
	Password   string `json:"password"`
	Mobile    string `json:"mobile"`
	//RealName    string `gorm:"column:real_name" json:"real_name"`
	Status int `json:"status"`
	//Token       string `json:"token"`
	//LastLoginIp string `gorm:"column:last_login_ip" json:"last_login_ip"`
}

func AddUser(user *Users) bool {
	err := variable.GormDbMysql.Create(&user).Error
	if err != nil {
		// todo 模型层和业务层log分开
		return false
	}
	return true
}

