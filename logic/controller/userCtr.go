package controller

import (
	"github.com/gin-gonic/gin"
	"nakeChat_fuckkkk/global/consts"
	"nakeChat_fuckkkk/global/variable"
	"nakeChat_fuckkkk/logic/model"
	"nakeChat_fuckkkk/utils/response"
)

/**
用户的功能接口
 */

// Login

// Register 注册(新增)用户
func Register(ctx *gin.Context) {
	var user model.Users // 声明接收的变量
	// ShouldBindJSON 将request的body中的数据，自动按照json格式解析到结构体
	if err := ctx.ShouldBindJSON(&user);err != nil {
		variable.ZapLog.Error(consts.ParseParamsFailMsg+err.Error())
		response.ErrorParamParse(ctx,user)
	}
	// TODO 新增之前先查询是否存在此用户，如果存在跳转到登陆页，跳转这动作应该由前端完成
	model.AddUser(&user)
}
// 发送信息
