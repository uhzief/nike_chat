package variable

import (
	"go.uber.org/zap"
	"gorm.io/gorm"
	"log"
	"nakeChat_fuckkkk/global/the_errors"
	"nakeChat_fuckkkk/utils/yml_conf/ymlConfInterf"
	"os"
	"strings"
)

var (
	BasePath string // 定义项目根目录
	ConfigKeyPrefix = "Config_" //  配置文件键值缓存时，键的前缀
	ZapLog *zap.Logger  // 全局日志配置文件
	ConfigYml ymlConfInterf.YmlConfInterface // 全局配置文件指针
	GormDbMysql *gorm.DB // 全局gorm的客户端连接
	WebsocketHub interface{} // WebsocketHub websocket 全局变量
)

func init() {
	//初始化程序根目录
	if path, err := os.Getwd(); err == nil {
		if len(os.Args) > 1 && strings.HasPrefix(os.Args[1], "-test") {
			BasePath = strings.Replace(strings.Replace(path, `\test`, "", 1), `/test`, "", 1)
		} else {
			BasePath = path
		}
	} else {
		log.Fatal(the_errors.ErrorsBasePath)
	}

}
