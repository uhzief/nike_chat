package response

import (
	"github.com/gin-gonic/gin"
	"nakeChat_fuckkkk/global/consts"
	"nakeChat_fuckkkk/global/the_errors"
	"net/http"
)


/**
语法糖函数封装
 */

// Success 直接返回成功Json
func Success(ctx *gin.Context, msg string, data interface{})  {
	ReturnJson(ctx,http.StatusOK,consts.CurdStatusOkCode, msg, data)
}

// MiddleFail 中间件里面有错误如果不想继续后续接口的调用不能直接return，而是应该调用c.Abort()方法。
func MiddleFail(ctx *gin.Context, dataCode int, msg string, data interface{})  {
	ReturnJson(ctx, http.StatusBadRequest, dataCode, msg, data)
	ctx.Abort()
}

// ErrorTokenAuthFail token 权限校验失败
func ErrorTokenAuthFail(c *gin.Context) {
	ReturnJson(c, http.StatusUnauthorized, http.StatusUnauthorized, the_errors.ErrorsNoAuthorization, "")
	//终止可能已经被加载的其他回调函数的执行
	c.Abort()
}

// ErrorCasbinAuthFail casbin 鉴权失败，返回 405--方法不允许访问
func ErrorCasbinAuthFail(c *gin.Context, msg interface{}) {
	ReturnJson(c, http.StatusMethodNotAllowed, http.StatusMethodNotAllowed, the_errors.ErrorsCasbinNoAuthorization, msg)
	c.Abort()
}

// ErrorParam 参数校验错误
func ErrorParam(c *gin.Context, wrongParam interface{}) {
	ReturnJson(c, http.StatusBadRequest, consts.ValidatorParamsCheckFailCode, consts.ValidatorParamsCheckFailMsg, wrongParam)
	c.Abort()
}

// ErrorParamParse 参数校验错误
func ErrorParamParse(c *gin.Context, wrongParam interface{}) {
	ReturnJson(c, http.StatusBadRequest, consts.ValidatorParamsCheckFailCode, consts.ValidatorParamsCheckFailMsg, wrongParam)
	c.Abort()
}

// ErrorSystem 系统执行代码错误
func ErrorSystem(c *gin.Context, msg string, data interface{}) {
	ReturnJson(c, http.StatusInternalServerError, consts.ServerOccurredErrorCode, consts.ServerOccurredErrorMsg+msg, data)
	c.Abort()
}




// ReturnJsonFromString 将json字符窜以标准json格式返回（例如，从redis读取json格式的字符串，返回给浏览器json格式）
func ReturnJsonFromString(Context *gin.Context, httpCode int, jsonStr string) {
	Context.Header("Content-Type", "application/json; charset=utf-8")
	Context.String(httpCode, jsonStr)
}

// ReturnJson 返回json格式数据
func ReturnJson(ctx *gin.Context, httpCode int, dataCode int, msg string, data interface{}) {
	ctx.JSON(httpCode,gin.H{
		"code":dataCode,
		"msg":msg,
		"data":data,
	})
}
