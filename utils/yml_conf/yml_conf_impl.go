package yml_conf

import (
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"log"
	"nakeChat_fuckkkk/core/container"
	"nakeChat_fuckkkk/global/variable"
	"nakeChat_fuckkkk/utils/yml_conf/ymlConfInterf"
	"time"
)

/**
yml接口实现
*/

type ymlConfig struct {
	viper *viper.Viper // 读配置库
}

// CreateYamlFactory 创建一个yml配置文件工厂
// 参数设置为可变参数的文件名，这样参数就可以不需要传递，如果传递了多个，我们只取第一个参数作为配置文件名
func CreateYamlFactory(fileName ...string) ymlConfInterf.YmlConfInterface {
	yamlConfig := viper.New()
	// 配置文件所在目录
	yamlConfig.AddConfigPath(variable.BasePath + "/config")
	// 需要读取的文件名,默认为：config
	if len(fileName) == 0 {
		yamlConfig.SetConfigName("config")
	} else {
		yamlConfig.SetConfigName(fileName[0])
	}
	//设置配置文件类型(后缀)为 yml
	yamlConfig.SetConfigType("yml")

	if err := yamlConfig.ReadInConfig(); err != nil {
		log.Fatal("..ddddddddd") //todo
	}
	return &ymlConfig{
		yamlConfig,
	}
}

// 由于 vipver 包本身对于文件的变化事件有一个bug，相关事件会被回调两次
// 常年未彻底解决，相关的 issue 清单：https://github.com/spf13/viper/issues?q=OnConfigChange
// 设置一个内部全局变量，记录配置文件变化时的时间点，如果两次回调事件事件差小于1秒，我们认为是第二次回调事件，而不是人工修改配置文件
// 这样就避免了 vipver 包的这个bug
var lastChangeTime time.Time

func init() {
	lastChangeTime = time.Now()
}

//监听文件变化
func (y *ymlConfig) ConfigFileChangeListen() {
	y.viper.OnConfigChange(func(changeEvent fsnotify.Event) {
		if time.Now().Sub(lastChangeTime).Seconds() >= 1 {
			if changeEvent.Op.String() == "WRITE" {
				y.clearCache()
				lastChangeTime = time.Now()
			}
		}
	})
	y.viper.WatchConfig()
}

// 清空已经窜换的配置项信息
func (y *ymlConfig) clearCache() {
	container.CreateContainersFactory().FuzzyDelete(variable.ConfigKeyPrefix)
}

// GetDuration 获取连接的最大存活时间
func (y *ymlConfig) GetDuration(keyName string) time.Duration {
	if y.isCache(keyName) {
		return y.getValueFromCache(keyName).(time.Duration)
	} else {
		value := y.viper.GetDuration(keyName)
		y.cache(keyName, value)
		return value
	}
}

func (y *ymlConfig) GetInt(keyName string) int {
	if y.isCache(keyName) {
		return y.getValueFromCache(keyName).(int)
	} else {
		value := y.viper.GetInt(keyName)
		y.cache(keyName, value)
		return value
	}
}

// GetString 获取缓存中 string 类型配置
func (y *ymlConfig) GetString(key string) string {
	if y.isCache(key) {
		return y.getValueFromCache(key).(string)
	} else {
		value := y.viper.GetString(key) // 否则的从配置读取信息
		y.cache(key, value)
		return value
	}
}

// GetBool 获取bool类型配置
func (y *ymlConfig)GetBool(key string) bool {
	if  y.isCache(key) {
		return y.getValueFromCache(key).(bool)
	} else {
		value := y.viper.GetBool(key)
		y.cache(key, value)
		return value
	}
}

/**
判断key是否已经缓存
*/
func (y *ymlConfig) isCache(key string) bool {
	if _, exist := container.CreateContainersFactory().KeyIsExists(variable.ConfigKeyPrefix + key); exist {
		return true
	}
	return false
}

/**
通过键获取缓存的值
*/
func (y *ymlConfig) getValueFromCache(key string) interface{} {
	return container.CreateContainersFactory().Get(variable.ConfigKeyPrefix + key)
}

/**
cache key->value
*/
func (y *ymlConfig) cache(key string, value interface{}) bool {
	return container.CreateContainersFactory().Set(variable.ConfigKeyPrefix+key, value)
}
