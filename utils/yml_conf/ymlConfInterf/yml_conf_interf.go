package ymlConfInterf

import "time"

/**
获取全局配置的所有接口
 */

type YmlConfInterface interface {
	GetString(keyName string) string
	GetInt(keyName string) int
	GetDuration(s string) time.Duration
	ConfigFileChangeListen()
	GetBool(s string) bool
}
