package websocket

import (
	"nakeChat_fuckkkk/global/variable"
	"net"
)

type Hub struct {
	//上线注册
	Register chan *Client
	//下线注销
	UnRegister chan *Client
	//所有在线客户端的内存地址
	Clients map[*Client]bool
}

// InitChatServe 初始化聊天服务
func InitChatServe() net.Listener {
	listener, err := net.Listen("tcp", "0.0.0.0:8888")
	if err!=nil {
		variable.ZapLog.Error("聊天服务初始化失败")
	}
	return listener
}

func CreateHubFactory() *Hub {
	return &Hub{
		Register:   make(chan *Client),
		UnRegister: make(chan *Client),
		Clients:    make(map[*Client]bool),
	}
}

func (h *Hub) Run()  {
	for {
		select {

		}
	}
}
