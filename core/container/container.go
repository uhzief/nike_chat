package container

import (
	"nakeChat_fuckkkk/global/the_errors"
	"nakeChat_fuckkkk/global/variable"
	"strings"
	"sync"
)

// 定义一个全局键值对存储容器,线程安全
var sMap sync.Map

// 定义一个容器结构体
type containers struct {
}

// CreateContainersFactory 创建一个容器工厂
func CreateContainersFactory() *containers {
	return &containers{}
}

// KeyIsExists 4. 判断键是否被注册
func (c *containers) KeyIsExists(key string) (interface{}, bool) {
	return sMap.Load(key)
}

// Get get value from map by key
func (c *containers) Get(key string) interface{} {
	if v, exist := c.KeyIsExists(key);exist {
		return v
	}
	return nil
}

// Set key->value to map
func (c *containers) Set(key string, value interface{}) bool {
	// 判断是否存在，不存在，则插入，否则警告
	if _, exist := c.KeyIsExists(key);exist {
		variable.ZapLog.Warn(the_errors.ErrorsContainerKeyAlreadyExists)
	}
	sMap.Store(key,value)
	return true
}

// FuzzyDelete 按照键的前缀模糊删除容器中注册的内容
func (c *containers) FuzzyDelete(keyPre string) {
	sMap.Range(func(key, value interface{}) bool {
		if keyname, ok := key.(string); ok {
			if strings.HasPrefix(keyname, keyPre) {
				sMap.Delete(keyname)
			}
		}
		return true
	})
}
