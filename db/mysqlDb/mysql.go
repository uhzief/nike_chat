package mysqlDb

import (
	"errors"
	"fmt"
	"go.uber.org/zap"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
	"nakeChat_fuckkkk/global/the_errors"
	"nakeChat_fuckkkk/global/variable"
	"strings"
	"time"
)

func GetMysqldb() (*gorm.DB, error) {
	sqlType := "Mysql"
	// 是偶开启读写分离，这里否，以后有需要再开启
	readDbIsOpen := variable.ConfigYml.GetInt(sqlType + ".IsOpenReadDb")
	return getSqlDriver(sqlType, readDbIsOpen)

}

// 获取数据库驱动, 可以通过options 动态参数连接任意多个数据库
func getSqlDriver(sqlType string, readDbIsOpen int, dbConf ...ConfigParams) (*gorm.DB, error) {
	var dbDialector gorm.Dialector
	// 初始化DbDialector
	dialector, err := getDbDialector(sqlType, "write", dbConf...)
	if err != nil {
		variable.ZapLog.Error(the_errors.ErrorsDialectorDbInitFail+sqlType, zap.Error(err))
	} else {
		dbDialector = dialector
	}
	gormDb, err := gorm.Open(dbDialector, &gorm.Config{ // 打开数据库连接
		SkipDefaultTransaction: true,
		PrepareStmt:            true,
		//Logger:                 redefineLog(sqlType), //拦截、接管 gorm 自带日志 // todo
	})
	if err != nil { // 连接出错
		return nil, err
	}
	variable.ZapLog.Info("数据库连接成功，，yes")
	// 如果开启了读写分离，配置读数据库（resource、read、replicas）
	// 连接读库
	if readDbIsOpen == 1 { // 配置开启了读写分离，需配置读库
		if diec, err := getDbDialector(sqlType, "write", dbConf...); err != nil {
			variable.ZapLog.Error(the_errors.ErrorsDialectorDbInitFail+sqlType, zap.Error(err))
		} else {
			dbDialector = diec
		}
		resolverConf := dbresolver.Config{
			Replicas: []gorm.Dialector{dbDialector}, //  读 操作库，查询类
			Policy:   dbresolver.RandomPolicy{},     // sources/replicas 负载均衡策略适用于
		}
		//
		err = gormDb.Use(dbresolver.Register(resolverConf).SetConnMaxIdleTime(time.Second * 30).
			SetConnMaxLifetime(variable.ConfigYml.GetDuration(sqlType+".Read.SetConnMaxLifetime") * time.Second).
			SetMaxIdleConns(variable.ConfigYml.GetInt( sqlType + ".Read.SetMaxIdleConns")).
			SetMaxOpenConns(variable.ConfigYml.GetInt(sqlType + ".Read.SetMaxOpenConns")))
		if err != nil {
			return nil, err
		}
	}
	// 查询没有数据，屏蔽 gorm v2 包中会爆出的错误
	// https://github.com/go-gorm/gorm/issues/3789  此 issue 所反映的问题就是我们本次解决掉的
	_ = gormDb.Callback().Query().Before("gorm:query").Register("disable_raise_record_not_found", func(d *gorm.DB) {
		d.Statement.RaiseErrorOnNotFound = false
	})

	// 为主连接设置连接池
	rawDb, err := gormDb.DB()
	if err != nil {
		return nil, err
	}
	rawDb.SetConnMaxIdleTime(variable.ConfigYml.GetDuration(sqlType+".Write.SetConnMaxIdleTime") * time.Second)
	rawDb.SetConnMaxLifetime(variable.ConfigYml.GetDuration(sqlType+".Write.SetConnMaxLifetime") * time.Second)
	rawDb.SetMaxIdleConns(variable.ConfigYml.GetInt(sqlType + ".Write.SetMaxIdleConns"))
	rawDb.SetMaxOpenConns(variable.ConfigYml.GetInt(sqlType + ".Write.SetMaxOpenConns"))
	return gormDb, nil
}

// 获取一个数据库方言(Dialector),通俗的说就是根据不同的连接参数，获取具体的一类数据库的连接指针
func getDbDialector(sqlType string, readWrite string, dbConf ...ConfigParams) (gorm.Dialector, error) {
	var dbDialector gorm.Dialector
	dsn := getDsn(sqlType, readWrite, dbConf...) // 获取连接数据源符串
	switch sqlType {
	case "Mysql":
		dbDialector = mysql.Open(dsn)
	// 下面的暂不配置
	//case "sqlserver":
	//	dbDialector = mysqlDb.Open(dsn)
	//case "postgres", "postgresql", "postgre":
	//	dbDialector = mysqlDb.Open(dsn)
	default:
		return nil, errors.New(the_errors.ErrorsDbDriverNotExists + sqlType)
	}
	return dbDialector, nil
}

//  根据配置参数生成数据库驱动 dsn
func getDsn(sqlType string, readWrite string, dbConf ...ConfigParams) string {
	Host := variable.ConfigYml.GetString(sqlType + "." + readWrite + ".Host")
	DataBase := variable.ConfigYml.GetString(sqlType + "." + readWrite+ ".DataBase")
	Port := variable.ConfigYml.GetInt(sqlType + "." + readWrite+ ".Port")
	User := variable.ConfigYml.GetString(sqlType + "." + readWrite+ ".User")
	Pass := variable.ConfigYml.GetString(sqlType + "." + readWrite+ ".Pass")
	Charset := variable.ConfigYml.GetString(sqlType + "." + readWrite+ ".Charset")

	if len(dbConf) > 0 {
		if strings.ToLower(readWrite) == "write" { // 如果配置的是写库
			if len(dbConf[0].Write.Host) > 0 {
				Host = dbConf[0].Write.Host
			}
			if len(dbConf[0].Write.DataBase) > 0 {
				DataBase = dbConf[0].Write.DataBase
			}
			if dbConf[0].Write.Port > 0 {
				Port = dbConf[0].Write.Port
			}
			if len(dbConf[0].Write.User) > 0 {
				User = dbConf[0].Write.User
			}
			if len(dbConf[0].Write.Pass) > 0 {
				Pass = dbConf[0].Write.Pass
			}
			if len(dbConf[0].Write.Charset) > 0 {
				Charset = dbConf[0].Write.Charset
			}
		} else { // 配置的是读库
			if len(dbConf[0].Read.Host) > 0 {
				Host = dbConf[0].Read.Host
			}
			if len(dbConf[0].Read.DataBase) > 0 {
				DataBase = dbConf[0].Read.DataBase
			}
			if dbConf[0].Read.Port > 0 {
				Port = dbConf[0].Read.Port
			}
			if len(dbConf[0].Read.User) > 0 {
				User = dbConf[0].Read.User
			}
			if len(dbConf[0].Read.Pass) > 0 {
				Pass = dbConf[0].Read.Pass
			}
			if len(dbConf[0].Read.Charset) > 0 {
				Charset = dbConf[0].Read.Charset
			}
		}
	}
	switch strings.ToLower(sqlType) {
	case "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=True&loc=Local", User, Pass, Host, Port, DataBase, Charset)
	case "sqlserver", "mssql":
		return fmt.Sprintf("server=%s;port=%d;database=%s;user id=%s;password=%s;encrypt=disable", Host, Port, DataBase, User, Pass)
	case "postgresql", "postgre", "postgres":
		return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=disable TimeZone=Asia/Shanghai", Host, Port, DataBase, User, Pass)
	}
	return ""
}

//// 创建自定义日志模块，对 gorm 日志进行拦截、
//func redefineLog(sqlType string) gormLog.Interface {
//	return createCustomGormLog(sqlType,
//		SetInfoStrFormat("[info] %s\n"), SetWarnStrFormat("[warn] %s\n"), SetErrStrFormat("[error] %s\n"),
//		SetTraceStrFormat("[traceStr] %s [%.3fms] [rows:%v] %s\n"), SetTracWarnStrFormat("[traceWarn] %s %s [%.3fms] [rows:%v] %s\n"), SetTracErrStrFormat("[traceErr] %s %s [%.3fms] [rows:%v] %s\n"))
//}
