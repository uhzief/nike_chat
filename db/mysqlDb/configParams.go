package mysqlDb

// 数据库参数配置，结构体
// 用于解决复杂的业务场景连接到多台服务器部署的 mysqlDb、sqlserver、postgresql 数据库

type ConfigParams struct {
	Write ConfigParamsDetail
	Read  ConfigParamsDetail
}

// ConfigParamsDetail 与配置文件的参数对应
type ConfigParamsDetail struct {
	Host     string
	DataBase string
	Port     int
	Prefix   string
	User     string
	Pass     string
	Charset  string
}